import logging
import os

import telegram
from telegram.ext import CommandHandler, Filters, MessageHandler, Updater

import votebotfiles as files

temp = ""
users = []
bottoken = "<your_bot_token>"
bot = telegram.Bot(token=bottoken)
updater = Updater(token=bottoken, use_context=True)
owner = "<your_user_id>"
dispatcher = updater.dispatcher


def stat(update, context):  # vote statistics
    output = ""
    user_id = update.effective_chat.id
    cout = context.bot.send_message
    if not files.authorised(str(user_id)):  # if user not authorised
        cout(
            chat_id=user_id,
            text="Seems like you are not authorised. Have you entered password in /password?",
        )
        return
    if files.checkexist("vote_data.txt"):  # if someone has voted
        il = 1
        while (
            il < 4
        ):  # change 4 to other values if you have more/less than 3 candidates
            count = 0
            file = open("vote_data.txt")
            str1 = "vote=" + str(il)
            for line in file:
                if str1 in line:
                    count += 1  # count number of specific votes
            il += 1
            output += str1 + ": " + str(count) + "\n"
            file.close()
        cout(chat_id=user_id, text=output)
        if str(user_id) == str(owner):  # extra information if user is owner
            il = 1
            output = ""
            while il < 4:
                file = open("vote_data.txt")
                str1 = "vote=" + str(il)
                for line in file:
                    if str1 in line:  # sort by vote
                        output += line  # fetch all data from file
                il += 1
                file.close()
            cout(chat_id=owner, text=output)
            return
        return
    cout(chat_id=user_id, text="No one has voted yet!")
    return


def users(update, context):  # display users only to the owner
    user_id = update.effective_chat.id
    cout = context.bot.send_message
    readableuserslist = ""
    if str(user_id) != str(owner):
        return
    try:
        infile = open("users_readable.txt")
        for line in infile:
            readableuserslist += line
        infile.close()
        cout(chat_id=owner, text=readableuserslist)
    except:
        return


def specific(update, context):  # keywords
    user_id = update.effective_chat.id
    cout = context.bot.send_message
    if "thank" in update.message.text and not (
        "love" in update.message.text or "nice to meet" in update.message.text
    ):
        cout(chat_id=user_id, text="My pleasure.")
        return
    if (
        "hi " in update.message.text
        or update.message.text == "hi"
        or "I am" in update.message.text
    ) and not ("love" in update.message.text or "nice to meet" in update.message.text):
        cout(chat_id=user_id, text="Nice to meet you!")
        return
    if "love" in update.message.text:
        cout(chat_id=user_id, text="But I don't.")
        return
    if "nice to meet" in update.message.text:
        cout(chat_id=user_id, text="Me too!")
        return
    cout(
        chat_id=user_id,
        text="I am a voting bot, not google assistant and I can't respond to every one of your messages.",
    )
    return


def ownerbroadcast(update, context):
    global users
    output = ""
    if str(update.effective_chat.id) != str(owner):
        return
    for i in range(16, len(update.message.text)):
        output += str(update.message.text)[i]
    if files.checkexist("users"):
        users = []
        infile = open("users")
        for line in infile:
            users.append(line)
        for u in users:
            try:
                bot.send_message(chat_id=u, text=output)
            except:
                pass
        return
    return


def password(update, context):
    context.bot.send_message(
        chat_id=str(update.effective_chat.id),
        text="Please enter your password. It must be in 8-digit and contains only numbers.",
    )


def init():
    global users
    if files.checkexist("users"):
        users = []
        infile = open("users")
        for line in infile:
            users.append(line)
        for u in users:
            try:
                bot.send_message(chat_id=u, text="Server up.")
            except:
                pass
        return
    return


def messageh(update, context):
    user_id = update.effective_chat.id
    filen = str(user_id) + "_changevote"
    if (
        update.message.text == "yes"
        or update.message.text == "no"
        and files.checkexist(filen)
    ):
        changevote(update, context)
    elif not (len(update.message.text) > 1) and update.message.text.isdigit():
        send(update, context)
    elif update.message.text.isdigit() and len(str(update.message.text)) == 8:
        passwordenter(update, context)
    elif "/ownerbroadcast" in update.message.text:
        ownerbroadcast(update, context)
    else:
        specific(update, context)


def restart(update, context):
    global users
    cout = context.bot.send_message
    if str(update.effective_chat.id) == owner:
        for u in users:
            try:
                bot.send_message(chat_id=u, text="Server down.")
            except:
                pass
        os.system("pkill python3 && python3 votebot.py")
    else:
        return


def down(update, context):
    global users
    cout = context.bot.send_message
    if str(update.effective_chat.id) == owner:
        for u in users:
            try:
                bot.send_message(chat_id=u, text="Server down.")
            except:
                pass
        os.system("pkill python3")
    else:
        return


def changevote(update, context):
    global temp
    user_id = update.effective_chat.id
    cout = context.bot.send_message
    filen = str(user_id) + "_changevote"
    if update.message.text == "yes" and files.checkexist(filen):
        os.remove(filen)
        newopen = open("vote_data_n.txt", "w")
        infile = open("vote_data.txt")
        if not files.checkexist("vote_data.txt"):
            files.createfile("vote_data.txt")
        for line in infile:
            if str(user_id) not in line and not line.isspace():
                newopen.write(line)
        output = (
            "vote="
            + str(temp)
            + ", first name="
            + str(update.message.from_user.first_name)
            + ", username="
            + str(update.message.from_user.username)
            + ", userid="
            + str(update.effective_chat.id)
        )
        cout(chat_id=owner, text=output)
        cout(chat_id=str(update.effective_chat.id), text="Done.")
        newopen.close()
        files.appendfile(filename="vote_data_n.txt", content=output)
        infile.close()
        os.remove("vote_data.txt")
        os.rename("vote_data_n.txt", "vote_data.txt")
        return
    if update.message.text == "no":
        os.remove(filen)
        cout(chat_id=user_id, text="Ok.")
    else:
        return


def send(update, context):
    global temp
    user_id = update.effective_chat.id
    cout = context.bot.send_message
    if len(update.message.text) > 1 or not update.message.text.isdigit():
        return
    if not files.authorised(user_id):
        return
    if (
        update.message.text != "1"
        and update.message.text != "2"
        and update.message.text != "3"
    ):
        cout(chat_id=str(update.effective_chat.id), text="Invalid!")
    else:
        print(files.checkexist(str(update.effective_chat.id)))
        if int(update.effective_chat.id) < 0:
            cout(
                chat_id=str(update.effective_chat.id),
                text="We don't receive votes from groups.",
            )
            return
        if files.checkexist(str(update.effective_chat.id)):
            temp = update.message.text
            cout = context.bot.send_message
            cout(
                chat_id=str(update.effective_chat.id),
                text="Seems like you have voted before. Do you want to change your vote? (yes/no)",
            )
            filen = str(user_id) + "_changevote"
            files.createfile(filen)
            return
        output = (
            "vote="
            + str(update.message.text)
            + ", first name="
            + str(update.message.from_user.first_name)
            + ", username="
            + str(update.message.from_user.username)
            + ", userid="
            + str(update.effective_chat.id)
            + "\n"
        )
        context.bot.send_message(chat_id=owner, text=output)
        cout(chat_id=str(update.effective_chat.id), text="Done.")
        files.appendfile(filename="vote_data.txt", content=output)
        files.createfile(str(update.effective_chat.id))


def votenow(update, context):
    user_id = update.effective_chat.id
    cout = context.bot.send_message
    if not files.authorised(user_id):
        cout(
            chat_id=user_id,
            text="Seems like you are not eligible. Have you entered password in /vote?",
        )
        return
    cout(chat_id=user_id, text="Please vote.")
    dispatcher.add_handler(MessageHandler(Filters.text, send))


def passwordenter(update, context):
    user_id = update.effective_chat.id
    if not (update.message.text.isdigit()) or len(str(update.message.text)) != 8:
        return
    if update.message.text != "<password>" and update.message.text != "/vote":
        cout = context.bot.send_message
        cout(chat_id=str(update.effective_chat.id), text="Incorrect.")
        cout(chat_id=str(update.effective_chat.id),
             text="Please enter your password.")
        return
    if update.message.text == "<password>":
        if files.checkexist("authorised_users"):
            appendf = "\n" + str(user_id)
        else:
            appendf = str(user_id)
        files.appendfile(filename="authorised_users", content=appendf)
        context.bot.send_message(
            chat_id=user_id, text="Done. You are now authorised.")
        return


def start(update, context):
    global users
    user_id = update.effective_chat.id
    cout = context.bot.send_message
    pass_var = False
    if files.checkexist("users"):
        infile = open("users")
        for line in infile:
            if str(user_id) in line:
                pass_var = True
                break
        infile.close()
        if pass_var:
            pass
        else:
            if files.checkexist("users"):
                appendf = "\n" + str(user_id)
            else:
                appendf = str(user_id)
            users.append(str(user_id))
            files.appendfile(filename="users", content=appendf)
    else:
        files.createfile("users")
        appendf = str(user_id) + "\n"
        files.appendfile(filename="users", content=appendf)
    if files.checkexist("users_readable.txt"):
        pass_var = False
        infile = open("users_readable.txt")
        for line in infile:
            if str(user_id) in line or int(user_id) < 0:
                pass_var = True
                break
        if pass_var:
            pass
        else:
            appendf = (
                "\n"
                + "first name="
                + str(update.message.from_user.first_name)
                + ", last name="
                + str(update.message.from_user.last_name)
                + ", username="
                + str(update.message.from_user.username)
                + ", userid="
                + str(user_id)
            )
            files.appendfile(filename="users_readable.txt", content=appendf)
        infile.close()
    else:
        files.createfile("users_readable.txt")
        appendf = (
            "first name="
            + str(update.message.from_user.first_name)
            + ", last name="
            + str(update.message.from_user.last_name)
            + ", username="
            + str(update.message.from_user.username)
            + ", userid="
            + str(user_id)
        )
        files.appendfile(filename="users_readable.txt", content=appendf)
    context.bot.send_message(
        chat_id=str(update.effective_chat.id), text="Type /help for commands"
    )


def help(update, context):
    if str(update.effective_chat.id) != str(owner):
        context.bot.send_message(
            chat_id=str(update.effective_chat.id),
            text="/vote: vote\n/stat: show voting statistics\n/password: type in password to authorise yourself",
        )
    else:
        context.bot.send_message(
            chat_id=str(update.effective_chat.id),
            text="/vote: vote\n/stat: show voting statistics\n/password type in password to authorise yourself\n/users: list users\n/ownerbroadcast <message>: broadcast <message>\n/restart: restart server\n/down: shut down server",
        )


def vote(update, context):
    user_id = update.effective_chat.id
    if files.authorised(str(user_id)):
        context.bot.send_message(
            chat_id=str(update.effective_chat.id),
            text="<what-are-you-voting-for>\nCandidates:\n1.<candidate1>\n2.<candidate2>\n3.<candidate3>",
        )
        context.bot.send_message(
            chat_id=str(update.effective_chat.id), text="enter /votenow to vote"
        )
    else:
        context.bot.send_message(
            chat_id=user_id,
            text="Seems like you are not eligible. Have you entered password in /password?",
        )


stat_handler = CommandHandler("stat", stat)
password_handler = CommandHandler("password", password)
restart_handler = CommandHandler("restart", restart)
down_handler = CommandHandler("down", down)
users_handler = CommandHandler("users", users)
start_handler = CommandHandler("start", start)
help_handler = CommandHandler("help", help)
vote_handler = CommandHandler("vote", vote)
votenow_handler = CommandHandler("votenow", votenow)
message_handler = MessageHandler(Filters.text, messageh)
dispatcher.add_handler(stat_handler)
dispatcher.add_handler(password_handler)
dispatcher.add_handler(down_handler)
dispatcher.add_handler(users_handler)
dispatcher.add_handler(votenow_handler)
dispatcher.add_handler(start_handler)
dispatcher.add_handler(restart_handler)
dispatcher.add_handler(help_handler)
dispatcher.add_handler(vote_handler)
dispatcher.add_handler(message_handler)
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
init()
updater.start_polling()
