A simple telegram voting bot based on the [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) api. <br />
# Prerequsites
- Debian-based linux distros, wsl works (other distros might also work, but have not yet tested), for installation, see below
- before proceeding, update and upgrade packages: <br />
```
sudo apt update
```
```
sudo apt upgrade
``` 
- python3 ,pip, git and nano installed:
```
sudo apt install python3 python3-pip git nano
```
- python-telegram-bot installed:
```
pip install python-telegram-bot --upgrade
```
## Installing ubuntu on Windows (WSL)
1. In the search bar, search "features", and select "add or remove windows features"
2. Select Windows Subsystem for Linux and Virtual machine platform in the list, then click ok
3. Restart your computer
4. In microsoft store, find ubuntu and install
5. You may also want to install windows terminal in microsoft store
6. open ubuntu.exe and finish setup
## Running ubuntu in a virtual machine
- You can use hyper-v, virtualbox, etc. for a desktop environment
- To install hyper-v on windows home version, see [my repository](https://gitlab.com/wcyat/enable-hyperv-sandbox)
## Installing ubuntu on your android phone
1. In [F-Droid](https://f-droid.org/), find and install userLAnd
2. select and setup ubuntu in userLAnd, use ssh for connection
3. run `sudo apt update` and `sudo apt upgrade`  
# Deploying the bot
1. Talk with [@botfather](https://t.me/botfather) in telegram to create a bot, and obtain its token
2. Clone the repository:
```
git clone https://gitlab.com/wcyat/telegram-votebot.git
```
3. Switch to directory:
``` 
cd telegram-votebot
```
4. Edit votebot.py:
```
nano votebot.py
```
Replace <> values with your own (eg. owner=your user id)<br />
5. start the bot:
```
python3 votebot.py
```
